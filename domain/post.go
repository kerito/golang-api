package domain

type Post struct {
	Id    int    `json:"id" db:"id"`
	Title string `json:"title" binding:"required"`
}

type PostUsecase interface {
	Create(post Post) (Post, error)
	GetAll() ([]Post, error)
	GetById(id int) (Post, error)
}

type PostRepository interface {
	Create(post Post) (Post, error)
	GetAll() ([]Post, error)
	GetById(id int) (Post, error)
}

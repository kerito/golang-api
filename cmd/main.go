package main

import (
	"fmt"
	"github.com/gorm/pkg/delivery"
	"github.com/gorm/pkg/repository"
	"github.com/gorm/pkg/usecase"
	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"net/http"
)

func main() {
	if err := initConfig(); err != nil {
		panic(fmt.Sprintf("error init configs : %s", err.Error()))
	}

	dbHost := viper.GetString("db.host")
	dbUser := viper.GetString("db.user")
	dbPass := viper.GetString("db.password")
	dbName := viper.GetString("db.name")
	dbPort := viper.GetString("db.port")
	dbSSL := viper.GetString("db.sslmode")
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s", dbHost, dbUser, dbPass, dbName, dbPort, dbSSL)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	repos := repository.NewRepository(db)
	usecases := usecase.NewUsecase(repos)
	handlers := delivery.NewHandler(usecases)

	srvPort := viper.GetString("server.port")
	if err := http.ListenAndServe(srvPort, handlers.InitRoutes()); err != nil {
		panic(err)
	}

}

func initConfig() error {
	viper.SetConfigFile("config.json")
	return viper.ReadInConfig()
}

package usecase

import (
	"github.com/gorm/domain"
	_postUsecase "github.com/gorm/internal/post/usecase"
	"github.com/gorm/pkg/repository"
)

type Usecase struct {
	Post domain.PostUsecase
}

func NewUsecase(repos *repository.Repository) *Usecase {
	return &Usecase{
		Post: _postUsecase.NewPostUsecase(repos.Post),
	}
}

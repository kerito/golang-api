package repository

import (
	"github.com/gorm/domain"
	_postRepository "github.com/gorm/internal/post/repository"
	"gorm.io/gorm"
)

type Repository struct {
	Post domain.PostRepository
}

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		Post: _postRepository.NewPostRepository(db),
	}
}

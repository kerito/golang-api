package delivery

import (
	"github.com/gin-gonic/gin"
	_postDelivery "github.com/gorm/internal/post/delivery"
	"github.com/gorm/pkg/usecase"
)

type Handler struct {
	usecases *usecase.Usecase
}

func NewHandler(usecases *usecase.Usecase) *Handler {
	return &Handler{usecases: usecases}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.Default()

	api := router.Group("/api")
	{
		_postDelivery.NewPostHandler(api, h.usecases.Post)
	}

	return router
}

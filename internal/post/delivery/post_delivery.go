package delivery

import (
	"github.com/gin-gonic/gin"
	"github.com/gorm/domain"
	"net/http"
	"strconv"
)

type PostHandler struct {
	PostUsecase domain.PostUsecase
}

func NewPostHandler(rg *gin.RouterGroup, us domain.PostUsecase) {

	h := &PostHandler{PostUsecase: us}

	posts := rg.Group("/posts")
	{
		posts.POST("/", h.createPost)
		posts.GET("/", h.getAllPosts)
		posts.GET("/:id", h.getPostById)
	}
}

func (h *PostHandler) createPost(c *gin.Context) {

	var input domain.Post

	if err := c.BindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, err.Error())
		return
	}
	post, err := h.PostUsecase.Create(input)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, post)
}

func (h *PostHandler) getAllPosts(c *gin.Context) {
	posts, err := h.PostUsecase.GetAll()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, posts)
}

func (h *PostHandler) getPostById(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, err.Error())
		return
	}

	post, err := h.PostUsecase.GetById(id)

	c.JSON(http.StatusOK, post)
}

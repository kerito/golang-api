package usecase

import "github.com/gorm/domain"

type PostUsecase struct {
	repo domain.PostRepository
}

func NewPostUsecase(repo domain.PostRepository) *PostUsecase {
	return &PostUsecase{repo: repo}
}

func (us *PostUsecase) Create(post domain.Post) (domain.Post, error) {
	return us.repo.Create(post)
}

func (us *PostUsecase) GetAll() ([]domain.Post, error) {
	return us.repo.GetAll()
}

func (us *PostUsecase) GetById(id int) (domain.Post, error) {
	return us.repo.GetById(id)
}

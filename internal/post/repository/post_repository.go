package repository

import (
	"github.com/gorm/domain"
	"gorm.io/gorm"
)

type PostRepository struct {
	db *gorm.DB
}

func NewPostRepository(db *gorm.DB) *PostRepository {
	return &PostRepository{db: db}
}

func (r *PostRepository) Create(post domain.Post) (domain.Post, error) {
	var newPost domain.Post
	row := r.db.Create(&post)
	if err := row.Scan(&newPost); err != nil {
		return newPost, err.Error
	}
	return newPost, nil
}

func (r *PostRepository) GetAll() ([]domain.Post, error) {
	var posts []domain.Post
	err := r.db.Find(&posts)
	return posts, err.Error
}

func (r *PostRepository) GetById(id int) (domain.Post, error) {
	var post domain.Post
	err := r.db.First(&post, id)
	return post, err.Error
}
